kibana:
  enabled: true
  image: mwppaas/opendistro-for-elasticsearch-kibana
  imageTag: 1.13.2
  imagePullPolicy: IfNotPresent
  replicas: 1
  port: 5601
  externalPort: 443
  resources:
    limits:
      cpu: 2
      memory: 2.5Gi
    requests:
      cpu: 0.5
      memory: 512Mi
  elasticsearchAccount:
    secret: es-account
    keyPassphrase:
      enabled: false
  ssl:
    kibana:
      enabled: false
      existingCertSecretCertSubPath: kibana-crt.pem
      existingCertSecretKeySubPath: kibana-key.pem
      existingCertSecretRootCASubPath: kibana-root-ca.pem
    elasticsearch:
      enabled: false
      existingCertSecretCertSubPath: elk-rest-crt.pem
      existingCertSecretKeySubPath: elk-rest-key.pem
      existingCertSecretRootCASubPath: elk-rest-root-ca.pem
  configDirectory: /usr/share/kibana/config
  certsDirectory: /usr/share/kibana/certs
  ingress:
    enabled: false
    path: /
    hosts:
      - chart-example.local
  service:
    type: ClusterIP
  serviceAccount:
    create: true
global:
  clusterName: elasticsearch
  es:
    username: admin
    password: admin
    port: 9200
  psp:
    create: true
  rbac:
    enabled: true
  imageRegistry: harbor.mwp-mavenir.com
elasticsearch:
  discoveryOverride: ""
  securityConfig:
    enabled: true
    path: /usr/share/elasticsearch/plugins/opendistro_security/securityconfig
  extraVolumes:
    - name: internal-users-vol
      secret:
        secretName: internal-users
  extraVolumeMounts:
    - name: internal-users-vol
      mountPath: /usr/share/elasticsearch/plugins/opendistro_security/securityconfig/internal_users.yml
      subPath: internal_users.yml
      readOnly: true
  initContainer:
    image: mwppaas/busybox
    imageTag: 1.27.2
  sysctl:
    enabled: true
  sys_chroot:
    enabled: true
  fixmount:
    enabled: true
  ssl:
    transport:
      existingCertSecretCertSubPath: elk-transport-crt.pem
      existingCertSecretKeySubPath: elk-transport-key.pem
      existingCertSecretRootCASubPath: elk-transport-root-ca.pem
    rest:
      enabled: false
      existingCertSecretCertSubPath: elk-rest-crt.pem
      existingCertSecretKeySubPath: elk-rest-key.pem
      existingCertSecretRootCASubPath: elk-rest-root-ca.pem
    admin:
      enabled: false
      existingCertSecretCertSubPath: admin-crt.pem
      existingCertSecretKeySubPath: admin-key.pem
      existingCertSecretRootCASubPath: admin-root-ca.pem
  master:
    enabled: true
    replicas: 1
    updateStrategy: RollingUpdate
    persistence:
      enabled: true
      subPath: ""
      accessModes:
        - ReadWriteOnce
      size: 8Gi
    resources:
      limits:
        cpu: 1
        memory: 4096Mi
      requests:
        cpu: 1
        memory: 4096Mi
    javaOpts: -Xms2g-Xmx2g
    podDisruptionBudget:
      enabled: false
      minAvailable: 1
    livenessProbe:
      tcpSocket:
        port: transport
      initialDelaySeconds: 120
      periodSeconds: 10
    timeoutSeconds: 5
  data:
    enabled: true
    dedicatedPod:
      enabled: true
    replicas: 1
    updateStrategy: RollingUpdate
    persistence:
      enabled: true
      subPath: ""
      accessModes:
        - ReadWriteOnce
      size: 30Gi
    resources:
      limits:
        cpu: 1
        memory: 4096Mi
      requests:
        cpu: 1
        memory: 4096Mi
    javaOpts: -Xms2g-Xmx2g
    podDisruptionBudget:
      enabled: false
      minAvailable: 1
    livenessProbe:
      tcpSocket:
        port: transport
      initialDelaySeconds: 120
      periodSeconds: 10
    timeoutSeconds: 5
  client:
    enabled: true
    dedicatedPod:
      enabled: true
    service:
      type: ClusterIP
    replicas: 1
    javaOpts: -Xms2g-Xmx2g
    ingress:
      enabled: false
      path: /
      hosts:
        - chart-example.local
    resources:
      limits:
        cpu: 1
        memory: 4096Mi
      requests:
        cpu: 1
        memory: 4096Mi
    podDisruptionBudget:
      enabled: false
      minAvailable: 1
    livenessProbe:
      tcpSocket:
        port: transport
      initialDelaySeconds: 120
      periodSeconds: 10
    timeoutSeconds: 5
  log4jConfig: ""
  loggingConfig:
    es:
      logger:
        level: INFO
    rootLogger: ${es.logger.level},console
    logger:
      action: DEBUG
      com:
        amazonaws: WARN
    appender:
      console:
        type: console
        layout:
          type: consolePattern
          conversionPattern: "[%d{ISO8601}][%-5p][%-25c]%m%n"
  transportKeyPassphrase:
    enabled: false
  sslKeyPassphrase:
    enabled: false
  maxMapCount: 262144
  image: mwppaas/opendistro-for-elasticsearch
  imageTag: 1.13.3
  imagePullPolicy: IfNotPresent
  configDirectory: /usr/share/elasticsearch/config
  serviceAccount:
    create: true
nameOverride: ""
fullnameOverride: ""
curator:
  enabled: true
  image:
    repository: harbor.mwp-mavenir.com/mwppaas/es-curator
    pullPolicy: IfNotPresent
    tag: 5.8.4
  history:
    successfulJobs: 1
    failedJobs: 3
  schedule: 0*/12***
  es_index_dateformat: "%Y.%m.%d"
  indicesUnit: days
  indicesAge: 2
  mountPath: /etc/config
  resources:
    requests:
      memory: 256Mi
      cpu: 100m
    limits:
      memory: 512Mi
      cpu: 200m
  elasticsearchAccount:
    secret: es-account
elasticsearch-exporter:
  enabled: true
  envFromSecret: es-account
  env:
    ES_URI: https://$(username):$(password)@$(esHost):$(esPort)
  image:
    repository: harbor.mwp-mavenir.com/mwppaas/elasticsearch_exporter
    tag: 1.1.0
    pullPolicy: IfNotPresent
    pullSecret: ""
  resources:
    limits:
      cpu: 100m
      memory: 128Mi
    requests:
      cpu: 100m
      memory: 128Mi
  serviceMonitor:
    enabled: true
  dashboard:
    enabled: false
    label: grafana_dashboard
  prometheusRule:
    enabled: true
    rules:
      - alert: OpendistroESDiskOutOfSpace
        annotations:
          description: "Thediskusageisover90%\r

            VALUE={{`{{`}}$value{{`}}`}}\r

            LABELS={{`{{`}}$labels{{`}}`}}"
          summary: Elasticsearchdiskoutofspace(instance{{`{{`}}$labels.instance{{`}}`}})
        expr: elasticsearch_filesystem_data_available_bytes/elasticsearch_filesystem_data_size_bytes*100<10
        for: 2m
        labels:
          severity: critical
      - alert: OpendistroESDiskSpaceLow
        annotations:
          description: "Thediskusageisover80%\r

            VALUE={{`{{`}}$value{{`}}`}}\r

            LABELS={{`{{`}}$labels{{`}}`}}"
          summary: Elasticsearchdiskspacelow(instance{{`{{`}}$labels.instance{{`}}`}})
        expr: elasticsearch_filesystem_data_available_bytes/elasticsearch_filesystem_data_size_bytes*100<20
        for: 2m
        labels:
          severity: warning
